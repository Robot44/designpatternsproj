﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WindowsFormsApplication1
{
    public class GameObject : ICloneable
    {
        public string Name { get; set; }

        public GameObject()
        {
            _components = new Dictionary<string, Component>();
        }

        protected Dictionary<string, Component> _components;

        public Dictionary<string, Component> Components
        {
            get { return _components; }
            set { _components = value; }
        }

        public void AddComponent(Component component)
        {
            component.Parent = this;
            _components[component.DerivedType()] = component;
        }

        public object Clone()
        {
            var obj = new GameObject();
            foreach (var component in Components.Reverse())
            {
                var val = component.Value;
                obj.AddComponent((Component)val.Clone());
            }
            return obj;
        }
    }
}