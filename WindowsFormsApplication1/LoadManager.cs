﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;

namespace WindowsFormsApplication1
{
    public class LoadManager
    {
        public int Level { get; set; } = 0;
        public void LoadLevel(List<GameObject> gameObjectsList)
        {
            GameObjectFactory fac = new GameObjectFactory();
            switch (Level)
            {
                case 0:
                    //player
                    var obj = fac.CreateGameObject(GameObjectFactory.GameObjectType.Player);
                    var obj2 = fac.CreateGameObject(GameObjectFactory.GameObjectType.Projectile);
                    for (int i = 0; i < 10; i++)
                    {
                        var p = (GameObject) obj2.Clone();
                        ((PlayerBehavior)obj.Components["Behavior"]).Projectiles.Add(p);
                        gameObjectsList.Add(p);
                    }
                    gameObjectsList.Add(obj);
                    //enemy
                    obj = fac.CreateGameObject(GameObjectFactory.GameObjectType.Enemy);
                    for (int i = 0; i < 10; i++)
                    {
                        var p = (GameObject)obj2.Clone();
                        ((EnemyShipBehavior)obj.Components["Behavior"]).Projectiles.Add(p);
                        gameObjectsList.Add(p);
                    }
                    gameObjectsList.Add(obj);
                    break;
            }
        }

        public void LoadMenu(List<GameObject> gameObjectsList)
        {
            var obj = new GameObject();
            obj.AddComponent(new Transform {Position = new Vector2(512, 256) , Size = 1f});
            obj.AddComponent(new Renderer {Visible = true,Sprite = new Bitmap (CreateMeAnImage("Control With Keyboard", 16, Color.BlueViolet))});
            obj.AddComponent(new ButtonBehavior { Command = new ControlWithKeyboard() });
            gameObjectsList.Add(obj);

            obj = new GameObject();
            obj.AddComponent(new Transform { Position = new Vector2(512, 356), Size = 1f });
            obj.AddComponent(new Renderer { Visible = true, Sprite = new Bitmap(CreateMeAnImage("Control With Mouse", 16, Color.BlueViolet)) });
            obj.AddComponent(new ButtonBehavior { Command = new ControlWithMouse() });
            gameObjectsList.Add(obj);

            obj = new GameObject();
            obj.AddComponent(new Transform {Position = new Vector2(512, 100), Size = 1f});
            obj.AddComponent(new Renderer { Visible = true, Sprite = new Bitmap(CreateMeAnImage("Menu", 32, Color.White)) });
            gameObjectsList.Add(obj);
        }

        private Image CreateMeAnImage(string text, int size, Color color)
        {
            Image img = new Bitmap(1, 1);
            Graphics drawing = Graphics.FromImage(img);
            SizeF textSize = drawing.MeasureString(text, new Font("Tahoma", size));
            img.Dispose();
            drawing.Dispose();
            img = new Bitmap((int)textSize.Width, (int)textSize.Height);
            drawing = Graphics.FromImage(img);
            drawing.Clear(color);
            Brush textBrush = new SolidBrush(Color.Black);
            drawing.DrawString(text, new Font("Tahoma", size), textBrush, 0, 0);
            drawing.Save();
            textBrush.Dispose();
            drawing.Dispose();
            return img;
        }

        public void LoadOver(List<GameObject> menuObjects)
        {
            menuObjects.Clear();
            var obj = new GameObject();
            obj.AddComponent(new Transform { Position = new Vector2(512, 256), Size = 1f });
            obj.AddComponent(new Renderer { Visible = true, Sprite = new Bitmap(CreateMeAnImage("Game Over", 16, Color.BlueViolet)) });
            menuObjects.Add(obj);
        }
    }
}