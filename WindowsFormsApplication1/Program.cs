﻿using System;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Game game = Game.Instance;
            game.SetGraphics(new FormGraphics(1024,768));
            //game.SetGraphics(new NullGraphics());
            game.GameLoop();
        }
    }
}
