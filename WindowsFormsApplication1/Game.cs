﻿using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public class Game
    {
        private static Game _instance;
        public static Game Instance => _instance ?? (_instance = new Game());

        [DllImport("user32.dll")]
        public static extern short GetAsyncKeyState(int x);

        public short[] Keyboard;
        public short[] PrevKeyboard;
        public readonly Timer Timer = new Timer();
        public MouseState MouseState;
        public IGraphics Graphics;
        public List<GameObject> GameObjects;
        public List<GameObject> MenuObjects;
        public Logger Log;
        private PlayerBehavior _behavior;

        private Game()
        {
            Log = ErrorLogger.Instance;
            Log.NextLogger = HighscoreLogger.Instance;
            var loadManager = new LoadManager();
            Keyboard = new short[256];
            GameObjects = new List<GameObject>();
            MenuObjects = new List<GameObject>();
            loadManager.LoadLevel(GameObjects);
            _behavior = GameObjects.Find(g => g.Name == "Player").Components[typeof(Behavior).Name] as PlayerBehavior;
            loadManager.LoadMenu(MenuObjects);
        }
        public void GameLoop()
        {
            var pause = false;
            var dead = false;
            Graphics.Display();
            while(Graphics.IsReady())
            {
                Timer.Pause = pause;
                Timer.Reset();
                ProcessInput();
                if (Instance.Keyboard[(int) Keys.Escape] == 2) pause = !pause;
                if (!_behavior.PlayerState.IsAlive())
                {
                    if (!dead)
                    {
                        dead = true;
                        var load = new LoadManager();
                        load.LoadOver(MenuObjects);
                    }
                    GameLogic(MenuObjects);
                    RenderScene(MenuObjects);
                }
                else if (!pause)
                {
                    GameLogic(GameObjects);
                    RenderScene(GameObjects);
                }
                else
                {
                    GameLogic(MenuObjects);
                    RenderScene(MenuObjects);
                }
                Application.DoEvents();
                while (Timer.GetTicks() < 15)
                {
                }
            }
        }

        private void RenderScene(List<GameObject> gameObjects )
        {
            Graphics.LoadBuffer();
            foreach (var obj in gameObjects)
            {
                var gameObject = obj;
                if (gameObject?.Components["Renderer"] == null) continue;
                var renderer = obj.Components["Renderer"] as Renderer;
                renderer?.Render(Graphics);
            }
            Graphics.Render();
        }
        private void ProcessInput()
        {
            PrevKeyboard = (short[])Keyboard.Clone();
            for (int x = 0; x < 256; x++)
            {
                bool val = (GetAsyncKeyState(x) & 0x8000) != 0;
                if (val == false & PrevKeyboard[x] == 1)
                {
                    Keyboard[x] = 2;
                }
                else if (val)
                {
                    Keyboard[x] = 1;
                }
                else
                {
                    Keyboard[x] = 0;
                }
            }
            MouseState = Graphics.GetMouseState();
        }
        private void GameLogic(List<GameObject> gameObjects)
        {
            foreach (GameObject obj in gameObjects)
            {
                if (obj.Components.ContainsKey("Behavior"))
                {
                    ((Behavior)obj.Components["Behavior"]).Update();
                }
            }
        }

        public void SetGraphics(IGraphics formGraphics)
        {
            Graphics = formGraphics;
        }
    }
}