﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace WindowsFormsApplication1
{
    public class PlayerFactory: Factory
    {
        private string projRoot =
           "c:/users/blue/documents/visual studio 2015/Projects/WindowsFormsApplication1/WindowsFormsApplication1/Assets/";
        public override GameObject CreateGameObject()
        {
            var bitmapList = new List<Bitmap>();
            try
            {
                bitmapList.Add(new Bitmap(projRoot + "Spaceship.png"));
            }
            catch (Exception)
            {
                Game.Instance.Log.LogMessage("Failed to load bitmaps at: " + projRoot, LogLevel.Error);
            }
            GameObject obj = new GameObject {Name = "Player"};
            obj.AddComponent(new PlayerBehavior {});
            obj.AddComponent(new Transform { Position = new Vector2(0, 0), Size = .5f });
            obj.AddComponent(new Renderer
            {
                Visible = true,
                Sprite = bitmapList[0]
            });
            obj.AddComponent(new Collider {Radius = 1f});
            return obj;
        }

    }
}