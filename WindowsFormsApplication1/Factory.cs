﻿namespace WindowsFormsApplication1
{
    public abstract class Factory
    {
        public abstract GameObject CreateGameObject();
    }
}