﻿using System;
using System.Security.Cryptography.X509Certificates;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public abstract class PlayerMovement
    {
        public abstract void Execute(Vector2 obj, Vector2 dir);
    }
    public class KeyboardMovevent : PlayerMovement
    {
        public override void Execute(Vector2 obj, Vector2 dir)
        {
            short[] input = Game.Instance.Keyboard;            
            if (input[(int)Keys.A] == 1)
            {
                dir.X -= 1;
            }
            if (input[(int)Keys.D] == 1)
            {
                dir.X += 1;
            }
            if (input[(int)Keys.W] == 1)
            {
                dir.Y -= 1;
            }
            if (input[(int)Keys.S] == 1)
            {
                dir.Y += 1;
            }
        }
    }
    public class MouseMovement : PlayerMovement
    {
        private bool _prev = false;
        public override void Execute(Vector2 obj, Vector2 dir)
        {
            dir.Y = 0;
            dir.X = 0;
            if (!Game.Instance.MouseState.MouseDown && !_prev)
            {
                _prev = Game.Instance.MouseState.MouseDown;
                return;
            }
            var ndir = new Vector2(Game.Instance.MouseState.PositinonVector2.X - obj.X,Game.Instance.MouseState.PositinonVector2.Y - obj.Y);
            if (obj.Distance(Game.Instance.MouseState.PositinonVector2) > 1)
            {
                ndir = ndir.NormalizedVector2();
                dir.X = ndir.X;
                dir.Y = ndir.Y;
            }
            _prev = Game.Instance.MouseState.MouseDown;
        }
    }
}