﻿using System.Drawing;

namespace WindowsFormsApplication1
{
    public class Player:IDrawable
    {
        public Point Position { get; set; }

        public Bitmap Sprite { get; set; }
        public void Draw(Graphics graphics)
        {
            if (Sprite != null)
            {
                graphics.DrawImage(Sprite,Position.X,Position.Y,Sprite.Width, Sprite.Height);
            }
        }

        public Player()
        {
        }
    }
}         