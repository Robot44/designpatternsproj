﻿using System.Drawing;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public interface IGraphics
    {
        /*
         * Returns mouse state on this graphic
         */
        MouseState GetMouseState();
        /*
         *Starts to display this graphic 
         */
        void Display();
        /*
         * Checks if graphics is ready to be written to
         */
        bool IsReady();
        /*
         * Draws sprite image to the buffer
         */
        void DrawSprite(Bitmap sprite, float correctedPosX, float correctedPosY, float f, float f1);
        /*Loads/Refreshes buffer
         */
        void LoadBuffer();
        /*
         * Display buffer content on the graphic
         */
        void Render();
    }

    public class MouseState
    {
        public Vector2 PositinonVector2 { get; set; }
        public bool MouseDown { get; set; }
        public bool MouseClick { get; set; }
    }
    class FormGraphics:IGraphics
    {
        private readonly Form _gameForm;
        public Vector2 MouseVector2 = new Vector2(0, 0);
        public bool MouseDown;
        public bool MouseClick;
        private readonly Graphics _graphics;
        private BufferedGraphics _buffer;
        private BufferedGraphicsContext _context;
        public FormGraphics(int width, int height)
        {
            _gameForm = new Form { Size = new Size(width, height) };
            _gameForm.MouseDown += GameMouseDownEventHandler;
            _gameForm.MouseUp += GameMouseUpEventHandler;
            _gameForm.MouseMove += GameMouseMoveEventHandler;
            _gameForm.MouseClick += GameMouseClickEventHandler;
            _graphics = _gameForm.CreateGraphics();
        }
        public MouseState GetMouseState()
        {
            var state = new MouseState { MouseClick = MouseClick, MouseDown = MouseDown, PositinonVector2 = MouseVector2 };
            MouseClick = false;
            return state;
        }

        public void Display()
        {
            _gameForm.Show();
        }

        public bool IsReady()
        {
            return _gameForm.Created;
        }

        public void DrawSprite(Bitmap sprite, float correctedPosX, float correctedPosY, float f, float f1)
        {
            var gr = _buffer.Graphics;
            gr.DrawImage(sprite, correctedPosX, correctedPosY,f, f1);
        }
        public void LoadBuffer()
        {
            _context = new BufferedGraphicsContext();
            _context = BufferedGraphicsManager.Current;
            _buffer = _context.Allocate(_gameForm.CreateGraphics(), _gameForm.DisplayRectangle);
            _buffer.Graphics.Clear(Color.White);
        }

        public void Render()
        {
            _buffer.Render();
            _buffer.Render(_graphics);
            _context.Dispose();
        }

        private void GameMouseMoveEventHandler(object sender, MouseEventArgs e)
        {
            MouseVector2.X = e.X;
            MouseVector2.Y = e.Y;
        }
        private void GameMouseDownEventHandler(object sender, MouseEventArgs e)
        {
            MouseDown = true;
        }
        private void GameMouseUpEventHandler(object sender, MouseEventArgs e)
        {
            MouseDown = false;
        }
        private void GameMouseClickEventHandler(object sender, MouseEventArgs e)
        {
            MouseClick = true;
        }
    }

    class NullGraphics : IGraphics {
        public MouseState GetMouseState()
        {
            return new MouseState();
        }

        public void Display()
        {
            
        }

        public bool IsReady()
        {
            return true;
        }

        public void DrawSprite(Bitmap sprite, float correctedPosX, float correctedPosY, float f, float f1)
        {
            
        }

        public void LoadBuffer()
        {
            
        }

        public void Render()
        {
           
        }
    }
}