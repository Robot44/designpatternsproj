﻿using System;
using System.Collections.Generic;
using Bitmap = System.Drawing.Bitmap;
using System.Linq;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public abstract class Component:ICloneable
    {
        public abstract string DerivedType();

        public GameObject Parent { get; set; }
        public abstract object Clone();
    }

    class Collider : Component
    {
        public float Radius { get; set; }
        public override string DerivedType()
        {
            return GetType().Name;
        }
        public override object Clone()
        {
            return new Collider {Radius = Radius};
        }
        public List<GameObject> CollidingWith()
        {
            var me = this.Parent;
            var collisions = new List<GameObject>();
            var tr = (Transform)Parent.Components[typeof(Transform).Name];
            foreach (var gameObject in Game.Instance.GameObjects)
            {
                if (gameObject == me || gameObject.Components["Collider"] == null) continue;
                var otherTr = (Transform)gameObject.Components[typeof(Transform).Name];
                var otherCollider = (Collider) gameObject.Components[typeof(Collider).Name];
                if (tr.Position.Distance(otherTr.Position)-Radius-otherCollider.Radius < 0)
                {
                    collisions.Add(gameObject);
                }
            }
            return collisions;
        }
    }

    class Transform : Component
    {
        public Vector2 Position { get; set; }

        public Vector2 Velocity { get; set; }

        public float Size { get; set; }

        public void Move(float x, float y)
        {
            Position.X += x;
            Position.Y += y;
        }

        public override string DerivedType()
        {
            return GetType().Name;
        }

        public override object Clone()
        {
            return new Transform { Position = (Vector2)Position.Clone(), Size = Size};
        }

        public bool Bounds(Vector2 pt)
        {
            var rend = (Renderer) Parent.Components[typeof(Renderer).Name];
            int width = rend.Sprite.Width;
            int height = rend.Sprite.Height;
            Vector2 rt = new Vector2(Position.X + width/2, Position.Y + height/2);
            Console.WriteLine(rt.X + @" " + rt.Y);
            Vector2 lb = new Vector2(Position.X - width/2, Position.Y - height/2);
            Console.WriteLine(lb.X + @" "+ lb.Y);
            return pt.X > lb.X && pt.X < rt.X && pt.Y < rt.Y && pt.Y > lb.Y;
        }
    }

    class Renderer : Component
    {
        public Bitmap Sprite { get; set; }

        public bool Visible { get; set; }

        internal void Render(IGraphics graphics)
        {
            if (Visible == false)
            {
                return;
            }
            var transform = (Transform) Parent.Components["Transform"];
            var correctedPos = new Vector2(transform.Position.X - (float)Sprite.Width/ 2 * transform.Size
                , transform.Position.Y - (float)Sprite.Height/ 2 * transform.Size);
            if (!float.IsNaN(transform.Position.X))
                graphics.DrawSprite(Sprite, correctedPos.X, correctedPos.Y, Sprite.Width * transform.Size, Sprite.Height * transform.Size);
        }

        public override string DerivedType()
        {
            return GetType().Name;
        }

        public override object Clone()
        {
            return new Renderer {Sprite = (Bitmap)Sprite.Clone(), Visible = false};
        }
    }

    public abstract class Behavior : Component
    {
        public abstract void Update();
        public override string DerivedType()
        {
            return "Behavior";
        }
    }

    class ProjectileBehavior : Behavior
    {
        public GameObject Owner { get; set; }
        private bool _active;

        private DateTime _activated;
        private DateTime _beActiveTill;

        public bool Active
        {
            get { return _active; }
            set
            {
                if (value)
                {
                    _activated = DateTime.Now;
                }
                ((Renderer) Parent.Components[typeof(Renderer).Name]).Visible = value;
                _beActiveTill = _activated + TimeSpan.FromSeconds(3);
                _active = value;
            }
        }

        public override void Update()
        {
            _activated += TimeSpan.FromSeconds(Game.Instance.Timer.DeltaTime());
            if (Active && _activated > _beActiveTill)
            {
                Console.WriteLine(@"projectile deactivated");
                Active = false;
                return;
            }
            if (Active)
            {
                var list = ((Collider)Parent.Components[typeof(Collider).Name]).CollidingWith();
                if (list.Count > 0)
                {
                    foreach (var gameObject in list)
                    {
                        if (gameObject.Name == "Player")
                        {
                            var playerBehavior = (PlayerBehavior)gameObject.Components[typeof(Behavior).Name];
                            playerBehavior.Health -= 200;
                            Console.WriteLine(@"collided with enemy");
                        }
                    }
                }
                var vel = ((Transform) Parent.Components[typeof(Transform).Name]).Velocity;
                ((Transform)Parent.Components[typeof(Transform).Name]).Move(vel.X * Game.Instance.Timer.DeltaTime(), vel.Y*Game.Instance.Timer.DeltaTime());
            }      
        }

        public override object Clone()
        {
            return new ProjectileBehavior();
        }
    }
       
    class PlayerBehavior : Behavior
    {
        public List<GameObject> Projectiles { get; set; } = new List<GameObject>();
        public PlayerMovement Movement { get; set; } = new MouseMovement();
        public int Health = 1000;
        public State PlayerState = new PlayerAliveState();

        public override void Update()
	    {
            Console.WriteLine(Health);
            PlayerState.Handle(this);
	    }

        public override object Clone()
        {
            return new PlayerBehavior();
        }
    }

    class EnemyShipBehavior : Behavior
    {
        public List<GameObject> Projectiles { get; set; } = new List<GameObject>();
        private DateTime _nextShot = DateTime.Now+ TimeSpan.FromSeconds(3);
        private DateTime _shotMade = DateTime.Now;
        private GameObject _player;
        public override void Update()
        {
            _shotMade += TimeSpan.FromSeconds(Game.Instance.Timer.DeltaTime());
            if (_shotMade > _nextShot && Projectiles.Count > 0)
            {
                if (_player == null)_player = Game.Instance.GameObjects.First(p => p.Name == "Player");
                foreach (GameObject t in Projectiles)
                {
                    if (((Renderer)t.Components[typeof(Renderer).Name]).Visible) continue;
                    _shotMade = DateTime.Now;
                    var myPos = ((Transform)Parent.Components[typeof(Transform).Name]).Position;
                    var playerPos = ((Transform)_player.Components["Transform"]).Position;
                    Vector2 vel = new Vector2(playerPos.X - myPos.X, playerPos.Y - myPos.Y);
                    vel = vel.NormalizedVector2().Magnify(200);
                    ((Transform)t.Components[typeof(Transform).Name]).Position = new Vector2(myPos.X, myPos.Y);
                    ((Transform) t.Components[typeof(Transform).Name]).Velocity = vel;
                    ((ProjectileBehavior)t.Components[typeof(Behavior).Name]).Active = true;
                    ((ProjectileBehavior)t.Components[typeof(Behavior).Name]).Owner = Parent;
                    _nextShot = DateTime.Now + TimeSpan.FromSeconds(2);
                    break;
                }
            }
        }

        public override object Clone()
        {
            return new EnemyShipBehavior();
        }
    }

    class ButtonBehavior : Behavior
    {
        public ICommand Command { get; set; }
        public override object Clone()
        {
            return new ButtonBehavior();
        }

        public override void Update()
        {
            if (Game.Instance.MouseState.MouseClick)
            {
                var tr = (Transform) Parent.Components[typeof(Transform).Name];
                if (tr.Bounds(Game.Instance.MouseState.PositinonVector2))
                {
                    Command.Execute();
                }
                else
                {
                    Console.WriteLine(@"out of bounds"+ Game.Instance.MouseState.PositinonVector2.X + @" " + Game.Instance.MouseState.PositinonVector2.Y);
                }
            }
        }
    }

    public class Vector2:ICloneable
    {
        public float X { get; set; }

        public float Y { get; set; }

        public Vector2(float x, float y)
        {
            X = x;
            Y = y;
        }

        public float Distance(Vector2 other)
        {
            return (float)Math.Sqrt(Math.Pow(other.X - X, 2)+Math.Pow(other.Y - Y,2));
        }

        public Vector2 NormalizedVector2()
        {
            float magnitude = (float)Math.Sqrt((X*X) + (Y*Y));
            return new Vector2(X / magnitude, Y / magnitude);
        }

        public object Clone()
        {
            return new Vector2(X,Y);
        }

        public Vector2 Magnify(float power)
        {
            X = X*power;
            Y = Y*power;
            return this;
        }
    }

    public abstract class State
    {
        public abstract void Handle(Behavior behavior);
        public abstract bool IsAlive();
    }

    public class PlayerAliveState:State
    {
        public override void Handle(Behavior behavior)
        {
            var playerBehavior = (PlayerBehavior) behavior;
            if (playerBehavior.Health > 0)
            {
                short[] input = Game.Instance.Keyboard;
                var deltaTime = Game.Instance.Timer.DeltaTime();
                Vector2 dir = new Vector2(0, 0);
                const int speed = 200;
                var pos = (Transform) (playerBehavior.Parent.Components[typeof(Transform).Name]);
                playerBehavior.Movement.Execute(pos.Position, dir);
                ((Transform) playerBehavior.Parent.Components["Transform"]).Move(dir.X*speed*deltaTime, dir.Y*speed*deltaTime);
                if (input[(int) Keys.Space] != 2) return;
                foreach (var t in playerBehavior.Projectiles)
                {
                    if (((Renderer) t.Components[typeof(Renderer).Name]).Visible) continue;
                    var myPos = ((Transform) playerBehavior.Parent.Components[typeof(Transform).Name]).Position;
                    var vel = new Vector2(0, -50);
                    ((Transform) t.Components[typeof(Transform).Name]).Position = new Vector2(myPos.X, myPos.Y);
                    ((Transform) t.Components[typeof(Transform).Name]).Velocity = vel;
                    ((ProjectileBehavior) t.Components[typeof(Behavior).Name]).Active = true;
                    ((ProjectileBehavior) t.Components[typeof(Behavior).Name]).Owner = playerBehavior.Parent;
                    break;
                }
            }
            else
            {
                playerBehavior.PlayerState = new PlayerDeadState {Died = DateTime.Now};
            }
        }

        public override bool IsAlive()
        {
            return true;
        }

        public class PlayerDeadState : State
        {
            public bool Changed = false;
            public DateTime Died;
            public override void Handle(Behavior behavior)
            {
                if (Changed) return;
                var projRoot =
                    "c:/users/blue/documents/visual studio 2015/Projects/WindowsFormsApplication1/WindowsFormsApplication1/Assets/";
                var playerBehavior = (PlayerBehavior) behavior;
                var renderrer = (Renderer) playerBehavior.Parent.Components[typeof(Renderer).Name];
                renderrer.Sprite = new Bitmap(projRoot + "Explosion.png");
                Changed = true;
                Console.WriteLine(@"player dead");
            }

            public override bool IsAlive()
            {
                //Console.WriteLine(DateTime.Now + " " + _died);
                return Died + TimeSpan.FromSeconds(5)> DateTime.Now;
            }
        }
    }
}