﻿using System;
using System.Resources;
using System.Runtime.InteropServices;

namespace WindowsFormsApplication1
{
    public class Timer
    {
        [DllImport("kernel32.dll")]
        private static extern long GetTickCount();
        private long _startTick = 0;

        private DateTime _prevTime;
        private DateTime _curTime;

        public bool Pause { get; set; } = false;

        public Timer()
        {
            _curTime = DateTime.Now;
            Reset();
        }

        public void Reset()
        {
            _prevTime = _curTime;
            _curTime = DateTime.Now;

            _startTick = GetTickCount();
        }

        public float DeltaTime()
        {
            if (Pause) return 0;
            return (float)(_curTime - _prevTime).TotalSeconds;
        }

        public long GetTicks()
        {
            long currentTick = 0;
            currentTick = GetTickCount();

            return currentTick - _startTick;
        }
    }
}