﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    interface ICommand
    {
        void Execute();
    }

    public class ControlWithKeyboard : ICommand
    {
        public void Execute()
        {
            var gameObject = Game.Instance.GameObjects.Find(p => p.Name == "Player");
            ((PlayerBehavior)gameObject.Components[typeof(Behavior).Name]).Movement = new KeyboardMovevent();
        }
    }

    public class ControlWithMouse : ICommand
    {
        public void Execute()
        {
            var gameObject = Game.Instance.GameObjects.Find(p => p.Name == "Player");
            ((PlayerBehavior)gameObject.Components[typeof(Behavior).Name]).Movement = new MouseMovement();
        }
    }
}
