﻿namespace WindowsFormsApplication1
{
    public class GameObjectFactory
    {
        public enum GameObjectType
        {
            Player,
            Enemy,
            Projectile
        }
        public GameObject CreateGameObject(GameObjectType type)
        {
            GameObject gameObject = null;
            Factory objFactory = null;
            switch (type)
            {
                case GameObjectType.Player:
                    objFactory = new PlayerFactory();
                    gameObject = objFactory.CreateGameObject();
                    break;
                case GameObjectType.Enemy:
                    objFactory = new EnemyFactory();
                    gameObject = objFactory.CreateGameObject();
                    break;
                case GameObjectType.Projectile:
                    objFactory = new ProjectileFactory();
                    gameObject = objFactory.CreateGameObject();
                    break;
            }
            return gameObject;
        }
    }
}