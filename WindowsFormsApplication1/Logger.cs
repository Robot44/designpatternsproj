﻿using System;
using System.Collections.Generic;
using System.IO;

namespace WindowsFormsApplication1
{
    public enum LogLevel
    {
        Error = 1,
        HighScore = 2,
    }
    public abstract class Logger
    {
        private readonly LogLevel _level;
        protected Logger(LogLevel level)
        {
            _level = level;
        }
        public Logger NextLogger { get; set; }
        public void LogMessage(string message, LogLevel level)
        {
            if (_level == level)
            {
                ProcessMessage(message);
            }
            NextLogger?.LogMessage(message,level);
        }
        public abstract void ProcessMessage(string message);
        public abstract List<String> ProcessRequest();
        public List<string> GetMessageList(LogLevel level)
        {
            if (_level == level)
            {
                return ProcessRequest();
            }
            return NextLogger?.GetMessageList(level);
        }
    }
    public class ErrorLogger : Logger
    {
        private static Logger _instance;
        private const string Path = @"c:/users/blue/documents/visual studio 2015/Projects/WindowsFormsApplication1/WindowsFormsApplication1/Assets/";
        private static readonly Object ThisLock = new Object();
        public static Logger Instance
        {
            get
            {
                lock (ThisLock)
                {
                    return _instance ?? (_instance = new ErrorLogger(LogLevel.Error));
                }
            }
        }
        private ErrorLogger(LogLevel level) : base(level) {}
        public override void ProcessMessage(string message)
        {
            if (!File.Exists(Path + "errors.txt"))
            {
                using (var sw = File.CreateText(Path))
                {
                    sw.WriteLine("Error log started: " + DateTime.Now);
                }
            }
            using (var sw = File.AppendText(Path + "errors.txt"))
            {
                sw.WriteLine(message + "| at " + DateTime.Now);
            }
        }
        public override List<string> ProcessRequest()
        {
            throw new NotImplementedException();
        }
    }
    public class HighscoreLogger : Logger
    {
        private static Logger _instance;
        private const string Path = @"c:/users/blue/documents/visual studio 2015/Projects/WindowsFormsApplication1/WindowsFormsApplication1/Assets/";
        private static readonly Object ThisLock = new Object();
        public static Logger Instance
        {
            get
            {
                lock (ThisLock)
                {
                    return _instance ?? (_instance = new HighscoreLogger(LogLevel.HighScore));
                }
            }
        }
        private HighscoreLogger(LogLevel level) : base(level) { }
        public override void ProcessMessage(string message)
        {
            if (!File.Exists(Path + "highscores.txt"))
            {
                using (var sw = File.CreateText(Path + "highscores.txt"))
                {
                    sw.WriteLine("Highscore log started: " + DateTime.Now);
                }
            }
            using (var sw = File.AppendText(Path + "highscores.txt"))
            {
                sw.WriteLine(message + "|" + DateTime.Now);
            }
        }

        public override List<string> ProcessRequest()
        {
            List<string> highscoresList = new List<string>();
            int counter = 0;
            using (var sr = new StreamReader(Path + "highscores.txt"))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    if (counter == 0)
                    {
                        counter++;
                        continue;
                    }
                    counter++;
                    highscoresList.Add(line);
                }
            }
            return highscoresList;
        }
    }
}