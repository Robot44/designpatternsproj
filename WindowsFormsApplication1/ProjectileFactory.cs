﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace WindowsFormsApplication1
{
    public class ProjectileFactory:Factory
    {
        private string projRoot =
           "c:/users/blue/documents/visual studio 2015/Projects/WindowsFormsApplication1/WindowsFormsApplication1/Assets/";
        public override GameObject CreateGameObject()
        {
            var bitmapList = new List<Bitmap>();
            try
            {
                bitmapList.Add(new Bitmap(projRoot + "Spaceship.png"));
            }
            catch (Exception)
            {
                Game.Instance.Log.LogMessage("Failed to load bitmaps at: " + projRoot,LogLevel.Error);
            }
            GameObject projectileGameObject = new GameObject();
            projectileGameObject.AddComponent(new ProjectileBehavior {});
            projectileGameObject.AddComponent(new Transform { Size = .5f, Position = new Vector2(0, 0) });
            projectileGameObject.AddComponent(new Collider { Radius = 10f });
            projectileGameObject.AddComponent(new Renderer
            {
                Visible = false,
                Sprite = bitmapList[0]
            });
            return projectileGameObject;
        }
    }
}